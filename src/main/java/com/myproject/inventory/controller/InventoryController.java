package com.myproject.inventory.controller;

import com.myproject.inventory.model.Inventory;
import com.myproject.inventory.model.Product;
import com.myproject.inventory.model.Store;
import com.myproject.inventory.repository.InventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/inventory")
public class InventoryController {

    @Autowired
    private InventoryRepository inventoryRepository;

    @GetMapping(path = "/all")
    public @ResponseBody
    Iterable<Inventory> getAllInventory() {
        return inventoryRepository.findAll();
    }

    @GetMapping(path = "/add")
    public @ResponseBody String addNewProduct (@RequestParam int productId,
                                                @RequestParam int storeId,
                                                @RequestParam double price,
                                               @RequestParam long quantity) {
        Inventory inventory = new Inventory();
        Product product = new Product();
        product.setId(productId);
        Store store = new Store();
        store.setId(storeId);
        inventory.setProduct(product);
        inventory.setStore(store);
        inventory.setPrice(price);
        inventory.setQuantity(quantity);

        inventoryRepository.save(inventory);
        return "Saved";
    }
}
