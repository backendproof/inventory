CREATE DATABASE db_myproject;
CREATE USER 'dbuser' IDENTIFIED BY 'password';
GRANT ALL on db_myproject.* TO 'dbuser';